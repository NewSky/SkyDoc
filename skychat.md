---
title  : Skychat Protocol
---

# Connection

## Recupération du jeton de connection

La connexion à un compte sur _redsky.fr_ nécessite un jeton de connexion. Celui-ci se récupère à l'aide d'une requête __POST__ à l'adresse _http://redsky.fr/ajax/account/api2.php_.

Les paramètres à envoyer sont :

* __pseudo__ : le pseudo du compte.
* __pass__ : le mot de passe du compte.

Après l'envoi de ces paramètres, le serveur renverra le jeton de connexion et d'autres informations de connexion en json. Ces informations ne doivent pas être modifiées avant leur renvoi au serveur SocketIO (partie suivante).

``` python
import requests
import json

ans = requests.post(
		"http://redsky.fr/ajax/account/api2.php", 
		{
			'pseudo' : "WRLD",
			'pass'   : "WhatDidYouExpect",
		}
)

credentials = json.loads(ans.text)
```

### Erreurs

Si les données envoyées sont inconnectes, le serveur peut envoyer plusieurs erreurs, sous la forme d'un json :

```json
{ "error" : "blabla" }
```

Les erreurs existantes sont :

* `"Donn\\u00e9es incomplaytes!"`, qui signifie qu'il manque des informations (le pseudo ou le mot de passe)
* `"Pass incorrecte. Tentative de connexion enregistr\\u00e9e"` indique que le mot de passe est incorrect pour ce compte
* `!pseudo` est renvoyé lorsque le pseudo indiqué n'existe pas. Cette erreur est particulière, car elle n'est pas renvoyée sous forme d'un json mais de sous forme de texte brut.


## Connection au serveur socketIO

Le serveur socketIO se trouve à l'adresse _redsky.fr_, port _8056_.

Pour s'y connecter, il faut envoyer un event de type _log_, avec pour contenu le dictionnaire obtenu à la section précédente.

Vous serez alors connecté au serveur, mais il vous restera à joindre une room pour pouvoir communiquer avec d'autres personnes. Pour cela, vous devez émettre un event de type _message_, avec pour valeur un dictionnaire `{ 'message' : '/join 0' }` _(en supposant que l'on veuille rejoindre la room principale)_.

``` python
from socketIO_client import SocketIO, LoggingNamespace

sock = SocketIO("redsky.fr", 8056, LoggingNamespace)
sock.emit('log', credentials)
sock.emit('message', { 'message' : "/join 0" }) 

# La socket sock est connectée et prête à servir
sock.emit('message', { 'message' : "Hello world !" })
```
\newpage

# Communication dans une room

Afin de communiquer, différents events sont employés par le serveur et les clients... en voici une liste non exhaustive (un Ø signifie que le message n'existe pas dans ce contexte) :

+-------------------+-----------------------------+-----------------------------+
| Event             | Réception                   | Émission                    |
+===================+=============================+=============================+
| _message_         | messages publics,           | messages,                   |
|                   | MP                          | commandes (/mp, /kick...)   |
+-------------------+-----------------------------+-----------------------------+
| _connected\_list_ | Liste des gens présents sur | $$ \emptyset $$             |
|                   | la room, renvoyée dès qu'   |                             |
|                   | une personne arrive ou part |                             |
+-------------------+-----------------------------+-----------------------------+
| _typing\_list_    | Liste des gens en train     | Indication que l'on         |
|                   | d'écrire                    | commence/arrete d'écrire    |
+-------------------+-----------------------------+-----------------------------+
| _pseudo\_info_    | Informations relatives à    | $$ \emptyset $$             |
|                   | son compte                  |                             |
+-------------------+-----------------------------+-----------------------------+
| _room\_update_    | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _mouse\_position_ | Liste de la position des    | Indication de la position   |
|                   | souris des gens actifs      | de sa propre souris         |
+-------------------+-----------------------------+-----------------------------+
| _log_             | $$ \emptyset $$             | Message de connection au    |
|                   |                             | serveur                     |
+-------------------+-----------------------------+-----------------------------+
| _log\_once_       | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _connect_         | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _disconnect_      | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _error_           | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _info_            | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _success_         | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _room\_list_      | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _room\_change_    | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _user\_left_      | __PAS IMPLÉMENTÉ__          | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _user\_joined_    | __PAS IMPLÉMENTÉ__          | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _yt\_room\_history_ | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _yt\_room\_waitlist | ???                         | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+
| _yt\_sync_        | Demande au serveur          | Indications sur la vidéo    |
|                   | d'émettre un _yt_sync_      | en train d'être diffusée    |
+-------------------+-----------------------------+-----------------------------+
| _yt\_search_      | $$ \emptyset $$             | ???                         |
+-------------------+-----------------------------+-----------------------------+
| _mouse\_destroy_  | __PAS IMPLÉMENTÉ__          | $$ \emptyset $$             |
+-------------------+-----------------------------+-----------------------------+

_D'autres types d'event existent surement, mais ils n'ont pas encore été implémentés, ou je n'ai pas encore eux besoin d'eux... Cette documentation sera mise à jour au fur et à mesure de l'évolution du SkyChat et de l'API._

\newpage

## Détails des event reçus

### 'pseudo\_info'

* `pseudo`                   : auteur du message
* `pseudo_lower`             : pseudo de l'auteur, en lowercase (minuscules seulement)
* `pseudo_html`              : représentation du pseudo de l'auteur, avec les masques et décorations
* `mask`                     : masque appliqué au pseudo (décoration), le `{p}` dans cette expression devra être remplacé par le pseudo
* `type`                     : nom du groupe de permission auquel appartient l'auteur (peut être _sandale_, ..., _mod_) `#TODO`
* `right`                    : _(integer)_ niveau de droit (nombre compris entre 0 et 100)
* `color`                    : couleur (au format #rrggbb) correspondant au niveau de droits)
* `mpable`                   : _(booléen)_ indique si l'auteur peut recevoir des mp
* `wizzable`                 : _(booléen)_ indique si l'auteur peut recevoir des wizz (_obsolète_)
* `client`                   : nature du client (_pc_, _bot_, ...) `#TODO`
* `logged`                   : _(booléen)_ indique si le client est connecté (sur un compte) ou non (on parle alors de _Hamster_)
* `points`                   : _(integer)_ nombre de _SkyDollars_ possédés par l'utilisateur
* `money`                    : _(integer)_ même chose que `points`
* `avatar`                   : url de l'avatar de l'utilisateur
* `current_room`             : _(integer)_ numéro de la _room_ actuelle
* `friend_list`              : __pas implémenté__
* `earn_money_buffer`        : _(integer)_ :risawat:
* `user_id`                  : _(integer)_ ID de l'utilisateur
* `id`                       : _(integer)_ ID
* `ip`                       : ipv6 du client
* `sandale_count`            : _(integer)_ compteur du nombre de sandales sur l'utilisateur
* `post_delay`               : _(float)_ délai minimum entre 2 posts
* `blacklist`                : liste des utilisateurs 
* `last_sent_mouse_position` : ...
* `last_message`             : ...
* `is_linked`                : _(booléen)_ indique certainement si le compte est lié a un compte jvc
* `currently_typing`         : _(booléen)_ ???
* `visible`                  : _(booléen)_ ???
* `cursor`                   : url de l'"avatar" du curseur de l'utilisateur
* `last_activity`            : _(float)_ timestamp du dernier moment où l'utilisateur a été actif sur le chat
* `tms_last_post`            : _(float)_ timestamp du dernier post de l'utilisateur
* `storage`                  : liste de variables stockées, à savoir :
	* blacklist
	* cursors   : liste des apparences de curseurs possédées
	* masks     : liste des masques de pseudos possédés

\newpage

### 'message'

_infos relatives à l'auteur_

Il s'agit des infos `pseudo`, `pseudo_lower`, `pseudo_html`, `mask`, `type`, `type`, `right`, `color`, `mpable`, `wizzable`, `client`, `logged` que l'on peut retrouver à la section ['pseudo_info'](#pseudo_info)

* `pseudo`        : auteur du message
* `pseudo_lower`  : pseudo de l'auteur, en lowercase (minuscules seulement)
* `pseudo_html`   : représentation du pseudo de l'auteur, avec les masques et décorations
* `mask`          : masque appliqué au pseudo (décoration), le `{p}` dans cette expression devra être remplacé par le pseudo
* `type`          : nom du groupe de permission auquel appartient l'auteur (peut être _sandale_, ..., _mod_) `#TODO`
* `right`         : _(integer)_ niveau de droit (nombre compris entre 0 et 100)
* `color`         : couleur (au format #rrggbb) correspondant au niveau de droits)
* `mpable`        : _(booléen)_ indique si l'auteur peut recevoir des mp
* `wizzable`      : _(booléen)_ indique si l'auteur peut recevoir des wizz (_obsolète_)
* `client`        : nature du client (_pc_, _bot_, ...) `#TODO`
* `logged`        : _(booléen)_ indique si le client est connecté (sur un compte) ou non (on parle alors de _Hamster_)

_message_

* `message`       : le message lui-même, en html
* `message_type ` : type de message, peut être :
	* `user_message` : message standard
	* `user_mp`      : message privé
* `id`            : _(integer)_ identifiant numérique du message, utilisé pour le citer, par exemple

_timestamp_

* `tms_dt`        : timestamp complet du message
* `tms_hr`        : _(integer)_ heure
* `tms_mn`        : _(integer)_ minute
* `tms_sc`        : _(integer)_ seconde

Un message peut aussi compoter les champs optionnels suivants :

* `old` : vaudra `True` si le message a été envoyé sur le chat avant la connection du client, inexistant sinon

\newpage

### 'connected\_list'

* `last_update` : _(float)_ date de la mise à jour de la liste
* `list`        : la liste elle-même, chaque élément désigne un client, avec :
	* les champs `pseudo`, `pseudo_lower`, `pseudo_html`, `type`, `color`, `right`, `avatar`, `points`, `money`,  sont similaires à ceux de la section ['pseudo\_info'](#pseudo_info)
	* `session_count` : _(integer)_ nombre de sessions actives du client
	* `room_admin`    : _(booléen)_ indique si l'utilisateur est administrateur de room (peut changer le background et le nom de la room
	* `last_activity` : _(float)_ indique le temps depuis lequel l'utilisateur n'a pas été vu actif (en secondes)
	* `xp`            : _(integer)_ expérience accumulée depuis la création du compte

### 'typing\_list'

* liste des pseudos des utilisateurs en train de taper, sous leur forme `pseudo_html`

### 'mouse\_position'

_lors d'un déplacement de curseur_

* `x` et `y` : _(float)_ coordonnées du curseur sur l'écran, compris entre 0 et 1
* `link`     : lien de l'"avatar" du curseur
* `pseudo`   : pseudo de l'utilisateur concerné

_lors d'une déconnexion_

* `died`   : _(booléen)_ vaut `True`
* `pseudo` : pseudo de l'utilisateur qui s'est déconnecté 

### 'yt\_sync'

* `thumb`    : raccourci vers la vignette de la vidéo
* `dj`       : pseudo de l'utilisateur qui a posté la vidéo
* `id`       : identifiant youtube de la vidéo
* `title`    : titre de la vidéo
* `cursor`   : _(integer)_ position dans la vidéo (en secondes)
* `duration` : _(integer)_ durée totale de la vidéo (en secondes)

\newpage

# Précisions

_Cette documentation n'a rien d'officielle, elle est issue de mes observations empiriques, donc il est possible qu'elle contienne des erreur. Si c'est le cas, merci de me les reporter._

_J'ai utilisé plusieurs mots anglais, afin d'éviter des traductions approximatives ou des mots batards de franglais tel que le_ ___mel___ _... Cela évite de trop s'éloigner des documentations officielles de socketIO, par exemple._

_Certains termes comme_ ___dictionnaire___ _, ainsi que les codes d'exemple employés, sont en python. Toutefois, j'ai tenté de rendre le code le plus simple possible, pour qu'il soit universellement compréhensible, même pour des développeurs ne maitrisant pas ce langage._ 
